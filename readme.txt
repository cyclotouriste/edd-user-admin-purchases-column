=== EDD User Admin Purchases Column ===
Contributors: mpol
Tags: easy digital downloads, edd, statistics, customer statistics
Requires at least: 4.1
Tested up to: 6.7
Stable tag: 1.0.5
License: GPLv2 or later
Requires PHP: 7.0

See basic statistics of customers for the Easy Digital Downloads e-commerce plugin.


== Description ==

See basic statistics of customers for the Easy Digital Downloads e-commerce plugin.
This plugin will show the number of orders a customer has made on the overview of the users table.
On the user profile you will see the number of orders, products bought and total amount spent.


= Translations =

Translations can be added very easily through [GlotPress](https://translate.wordpress.org/projects/wp-plugins/edd-user-admin-purchases-column).
You can start translating strings there for your locale. They need to be validated though, so if there's no validator yet, and you want to apply for being validator (PTE), please post it on the support forum.
I will make a request on make/polyglots to have you added as validator for this plugin/locale.

= Contributions =

This plugin is also available in [Codeberg](https://codeberg.org/cyclotouriste/edd-user-admin-purchases-column).


== Installation ==

= Installation =

* Install the plugin through the admin page "Plugins".
* Alternatively, unpack and upload the contents of the zipfile to your '/wp-content/plugins/' directory.
* Activate the plugin through the 'Plugins' menu in WordPress.
* That's it.


== Frequently Asked Questions ==

= I want to see more statistics than this =

You could use the [User History](https://easydigitaldownloads.com/downloads/user-history/) plugin.

= I want to translate this plugin =

Translations can be added very easily through [GlotPress](https://translate.wordpress.org/projects/wp-plugins/edd-user-admin-purchases-column).
You can start translating strings there for your locale.
They need to be validated though, so if there's no validator yet, and you want to apply for being validator (PTE), please post it on the support forum.
I will make a request on make/polyglots to have you added as validator for this plugin/locale.



== Screenshots ==

1. Test...


== Changelog ==

= 1.0.5 =
* 2024-11-22
* Remove plugin dependencies, so it can be used with EDD Pro as well.

= 1.0.4 =
* 2024-10-02
* Use __DIR__ instead of dirname(__FILE__).
* Loading plugin translations should be delayed until init action (in this case admin_init).

= 1.0.3 =
* 2024-03-05
* Add plugin dependencies for WP 6.5.

= 1.0.2 =
* 2021-11-30
* Some updates from phpcs and wpcs.

= 1.0.1 =
* 2021-06-01
* Support translations.

= 1.0.0 =
* 2021-05-21
* Initial release.
