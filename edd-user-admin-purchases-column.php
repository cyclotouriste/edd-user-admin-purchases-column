<?php
/*
Plugin Name: EDD User Admin Purchases Column
Plugin URI: https://wordpress.org/plugins/edd-user-admin-purchases-column/
Description: See basic statistics of customers for the Easy Digital Downloads e-commerce plugin.
Version: 1.0.5
Author: Marcel Pol
Author URI: https://timelord.nl
License: GPLv2 or later
Text Domain: edd-user-admin-purchases-column
Domain Path: /lang/


Copyright 2021 - 2024  Marcel Pol  (marcel@timelord.nl)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


// Plugin Version
define('EDD_UAPC_VER', '1.0.5');


/*
 * Adds Purchases Column To Users List Table.
 * The format is: `'internal-name' => 'Title'`
 *
 * @param  array $columns List of columns.
 *
 * @return array $columns List of columns.
 *
 * @since 1.0.0
 */
function edd_uapc_manage_users_columns( $columns ) {

	if ( function_exists( 'edd_get_users_purchases' ) ) {
		$columns['edd_uapc'] = esc_attr__( 'Purchases', 'edd-user-admin-purchases-column' );
	}

	return $columns;

}
add_filter( 'manage_users_columns', 'edd_uapc_manage_users_columns' );


/*
 * Adds Number of purchases To The Added Column.
 *
 * @param $value       string Custom column output. Default empty.
 * @param $column_name string Column name.
 * @param $user_id     int    ID of the currently-listed user.
 *
 * @return $value      string Number of purchases made in EDD in case there are any.
 *
 * @uses edd_get_users_purchases() from easy-digital-downloads plugin.
 *
 * @since 1.0.0
 */
function edd_uapc_manage_users_custom_column( $value, $column_name, $user_id ) {

	if ( 'edd_uapc' === $column_name ) {
		if ( function_exists( 'edd_get_users_purchases' ) ) {

			// edd_get_users_purchases( $user = 0, $number = 20, $pagination = false, $status = 'complete' );
			// @return WP_Post[]|false List of all user purchases.

			$purchases = edd_get_users_purchases( $user_id, -1, false, 'any' );
			if ( is_array( $purchases ) ) {
				$count = count( $purchases );
				return (string) $count;
			}

		}
	}

	return $value; // always return, otherwise we overwrite stuff from other plugins.

}
add_filter( 'manage_users_custom_column', 'edd_uapc_manage_users_custom_column', 10, 3 );


/*
 * Adds Number of purchases To The User Profile.
 *
 * @param object $user Instance of the currently-listed user.
 *
 * @uses multiple functions from easy-digital-downloads plugin.
 *
 * @since 1.0.0
 */
function edd_uapc_manage_users_profile_info( $user ) {

	if ( function_exists( 'edd_get_users_purchases' ) ) {

		$user_id = $user->ID;

		// edd_get_users_purchases( $user = 0, $number = 20, $pagination = false, $status = 'complete' );
		// @return WP_Post[]|false List of all user purchases.
		$purchases = edd_get_users_purchases( $user_id, -1, false, 'any' );
		$purchases_count = 0;
		if ( is_array( $purchases ) ) {
			$purchases_count = count( $purchases );
		}
		$purchases_description = sprintf( _n( '%d Purchase made.', '%d Purchases made.', $purchases_count, 'edd-user-admin-purchases-column' ), $purchases_count );

		$purchases = edd_get_users_purchases( $user_id, -1, false, 'complete' );
		if ( is_array( $purchases ) ) {
			$purchases_count = count( $purchases );
			$purchases_description .= ' ' . sprintf( _n( '(%d complete)', '(%d complete)', (int) $purchases_count, 'edd-user-admin-purchases-column' ), (int) $purchases_count );
		}
		$purchases_description .= '.';


		// edd_get_users_purchased_products( $user = 0, $status = 'complete' );
		// @return WP_Post[]|false List of unique products purchased by user.
		$products = edd_get_users_purchased_products( $user_id, 'any' );
		$products_count = 0;
		if ( is_array( $products ) ) {
			$products_count = count( $products );
		}
		$products_description = sprintf( _n( '%d Product ordered.', '%d Products ordered.', (int) $products_count, 'edd-user-admin-purchases-column' ), (int) $products_count );


		// edd_purchase_total_of_user( $user = null );
		// @return float - the total amount the user has spent.
		$spent = edd_purchase_total_of_user( $user_id );
		$spent = edd_format_amount( $spent, true );
		$spent = edd_currency_filter( $spent );
		$spent_description = sprintf( esc_html__( '%s Spent.', 'edd-user-admin-purchases-column'), $spent );


		?>
		<table class="form-table edd-uapc">
			<tbody>
				<tr>
					<th>
						<label for="edd_uapc"><?php esc_html_e('EDD Purchases', 'edd-user-admin-purchases-column' ); ?></label>
					</th>
					<td>
						<span class="description"><?php echo $purchases_description; ?></span><br />
						<span class="description"><?php echo $products_description; ?></span><br />
						<span class="description"><?php echo $spent_description; ?></span><br />
					</td>
				</tr>
			<tbody>
		</table>
		<?php
	}

}
add_action( 'show_user_profile', 'edd_uapc_manage_users_profile_info' ); // editing your own profile
add_action( 'edit_user_profile', 'edd_uapc_manage_users_profile_info' ); // editing another user


/*
 * Load Language files for dashboard only.
 *
 * @since 1.0.1
 */
function edd_uapc_load_lang() {

	load_plugin_textdomain( 'edd-user-admin-purchases-column', false, __DIR__ . '/lang' );

}
add_action( 'admin_init', 'edd_uapc_load_lang' );
